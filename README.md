# jboss_eap6-cookbook

Installs/Configures JBoss EAP 6 (JBoss AS 7) using comminity installer from jboss.org

The current release is 0.1.0. This is still under heavy development and is not intended for production use.

## Supported Platforms

CentOS 6.x x86_64

## Attributes

Required to be set by user:

default['jboss_eap6']['installer_url']  - URL to download JAR file from

The rest of the attributes have defaults set:

default['jboss_eap6']['base_dir'] - Base directory where JBoss will be installed - Default: "/opt/jboss"

default['jboss_eap6']['jboss_user'] - User that JBoss will run as - Default: "jboss"

default['jboss_eap6']['admin_password_databag'] - Name of data bag that contains JBoss admin password - Default: "jboss_eap6"

default['jboss_eap6']['admin_password_databag_item'] - Name of data bag item that contains JBoss admin password - Default: "credentials"

default['jboss_eap6']['java_package'] - Name of java package to install - Default: "java-1.7.0-openjdk"

default['jboss_eap6']['install_version'] - Version of EAP package - Default: "jboss-eap-6.3"

default['jboss_eap6']['installer_name'] - Name of jar install file - Default: "jboss-eap-6.3.0-installer.jar"


## Data bags

Requires an encrypted data bag item that contains the JBoss admin password. 
By default, the recipe will look for a data bag/item called jboss_eap6/credentials. This can be overridden via attribute.
The key name is 'password'

## Usage

### jboss_eap6::default

Include `jboss_eap6` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[jboss_eap6::default]"
  ]
}
```

## Contributing

1. Fork the repository on Bitbucket
2. Create a named feature branch (i.e. `add-new-recipe`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request

## License and Authors

Author:: John Byrne (johnpbyrne@gmail.com)
