#
# Cookbook Name:: jboss_eap6
# Recipe:: default
#
# Copyright (C) 2014 John Byrne
#
# All rights reserved - Do Not Redistribute
#

# *** TO DO LIST ***
# - Moar attributes!
# - Documentation!

include_recipe 'jboss_eap6::install_jar'
include_recipe 'jboss_eap6::config_service_initd'