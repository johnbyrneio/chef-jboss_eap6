default['jboss_eap6']['base_dir']     = "/opt/jboss"
default['jboss_eap6']['jboss_user']   = "jboss"

default['jboss_eap6']['admin_password_databag']		 = "jboss_eap6"
default['jboss_eap6']['admin_password_databag_item'] = "credentials"

default['jboss_eap6']['java_package'] = "java-1.7.0-openjdk"

default['jboss_eap6']['install_version'] = "jboss-eap-6.3"
default['jboss_eap6']['installer_name']  = "jboss-eap-6.3.0-installer.jar"
default['jboss_eap6']['installer_url']   = "https://s3.amazonaws.com/jboss-eap6/jboss-eap-6.3.0-installer.jar"


