#
# Cookbook Name:: jboss_eap6
# Recipe:: config_service_initd
#
# Copyright (C) 2014 - John Byrne
#
# All rights reserved - Do Not Redistribute
#

# Create directory /etc/jboss-as
directory "/etc/jboss-as"

# Create /etc/jboss-as/jboss-as.conf from template
template "/etc/jboss-as/jboss-as.conf" do
	source "jboss-as.conf.erb"
	notifies :restart, "service[jboss-as]", :delayed
end

# Create /etc/rc.d/init.d/jboss-as from template
template "/etc/rc.d/init.d/jboss-as" do
	source "jboss-as-standalone.sh.erb"
	mode 0755
end

# Enable and start JBoss AS service
service "jboss-as" do
	supports :start => true, :restart => true, :reload => true
	action [ :enable, :start ]
end