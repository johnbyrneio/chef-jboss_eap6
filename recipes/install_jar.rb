#
# Cookbook Name:: jboss_eap6
# Recipe:: install_jar
#
# Copyright (C) 2014 - John Byrne
#
# All rights reserved - Do Not Redistribute
#

# Install java-openjdk
package node['jboss_eap6']['java_package']

# Create jboss user that will own the install directory
user node['jboss_eap6']['jboss_user'] do
	action :create
	comment "JBoss AS Service Account"
	shell "/sbin/nologin"
	system true
end

# Create base directory where JBoss will live.
directory node['jboss_eap6']['base_dir'] do
	owner node['jboss_eap6']['jboss_user']
end

# Begin install block. Will skip if already installed
unless ::File.exists?("#{node['jboss_eap6']['base_dir']}/#{node['jboss_eap6']['install_version']}")
	# Download jar installer from node[jboss_eap6][installer_url]
	remote_file "#{Chef::Config[:file_cache_path]}/#{node['jboss_eap6']['installer_name']}" do
		source node['jboss_eap6']['installer_url']
	end

	# Create auto.xml from template. Place in Chef cache path
	template "#{Chef::Config[:file_cache_path]}/auto.xml" do
		source "auto.xml.erb"
	end

	# Create auto.xml.variables from template. Load admin password from databag.
	jboss_admin = Chef::EncryptedDataBagItem.load(node['jboss_eap6']['admin_password_databag'], node['jboss_eap6']['admin_password_databag_item'])

	template "#{Chef::Config[:file_cache_path]}/auto.xml.variables" do
		source "auto.xml.variables.erb"
		variables({
			:admin_password => jboss_admin['password']
			})
	end

	# Execute java -jar <jar installer> <path to auto.xml> -variablefile <path to auto.xml.variables>
	execute "Installing JBoss EAP 6" do
		command "java -jar #{Chef::Config[:file_cache_path]}/#{node['jboss_eap6']['installer_name']} #{Chef::Config[:file_cache_path]}/auto.xml -variablefile #{Chef::Config[:file_cache_path]}/auto.xml.variables"
		user node['jboss_eap6']['jboss_user']
		not_if { ::File.exists?("#{node['jboss_eap6']['base_dir']}/#{node['jboss_eap6']['install_version']}") }
	end

	# Symlink /usr/share/jboss-as to installation directory
	link "/usr/share/jboss-as" do
		to 		"#{node['jboss_eap6']['base_dir']}/#{node['jboss_eap6']['install_version']}"
		owner	node['jboss_eap6']['jboss_user']
	end

	# Post-install cleanup
	file "#{Chef::Config[:file_cache_path]}/#{node['jboss_eap6']['installer_name']}" do
		action :delete
	end

	file "#{Chef::Config[:file_cache_path]}/auto.xml" do
		action :delete
	end

	file "#{Chef::Config[:file_cache_path]}/auto.xml.variables" do
		action :delete
	end
end

# Replace standalone.xml with one from template. Currently only changes bind addressses from 127.0.0.1 to 0.0.0.0
template "#{node['jboss_eap6']['base_dir']}/#{node['jboss_eap6']['install_version']}/standalone/configuration/standalone.xml" do
	source	"standalone.xml.erb"
	owner	node['jboss_eap6']['jboss_user']
	notifies :restart, "service[jboss-as]", :delayed
end

service "jboss-as" do
	supports :restart => true
	action	 :nothing
end